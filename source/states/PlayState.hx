package states;

import nme.Lib;
import org.flixel.FlxG;
import org.flixel.FlxState;
import org.flixel.FlxButton;
import org.flixel.FlxText;
import org.flixel.FlxGroup;
import org.flixel.FlxSprite;

class PlayState extends FlxState
{
	// paddles
	private var paddle1:Paddle;
	private var paddle2:Paddle;
	private var paddles:FlxGroup;

	// walls
	private var wallLeftTop:Walls;
	private var wallLeftBottom:Walls;
	private var wallRightTop:Walls;
	private var wallRightBottom:Walls;
	private var wallTop:Walls;
	private var wallBottom:Walls;
	private var walls:FlxGroup;

	// ball
	private var ball:Ball;

	override public function create():Void
	{
		#if !neko
		FlxG.bgColor = 0xff131c1b;
		#else
		FlxG.camera.bgColor = {rgb: 0x131c1b, a: 0xff};
		#end		
		#if !FLX_NO_MOUSE
		FlxG.mouse.show();
		#end

		// create paddles
		paddle1 = new Paddle(Std.int(10), Std.int(FlxG.height / 2 - 40), 1);
		paddle2 = new Paddle(Std.int(FlxG.width - 30), Std.int(FlxG.height / 2 - 40), 2);

		paddles = new FlxGroup();
		paddles.add(paddle1);
		paddles.add(paddle2);

		// add walls
		wallLeftTop 	= new Walls(Std.int(0), Std.int(0), "side");
		wallLeftBottom 	= new Walls(Std.int(0), Std.int(FlxG.height - 130), "side");

		wallRightTop 	= new Walls(Std.int(FlxG.width - 20), Std.int(0), "side");
		wallRightBottom = new Walls(Std.int(FlxG.width - 20), Std.int(FlxG.height - 130), "side");

		wallTop 	= new Walls(Std.int(0), Std.int(0), "top-bottom");
		wallBottom 	= new Walls(Std.int(0), Std.int(FlxG.height - 20), "top-bottom");

		walls = new FlxGroup();
		walls.add(wallLeftTop);
		walls.add(wallLeftBottom);
		walls.add(wallRightTop);
		walls.add(wallRightBottom);
		walls.add(wallTop);
		walls.add(wallBottom);

		// the ball
		ball = new Ball(Std.int(FlxG.width/2 - 16), Std.int(FlxG.height/2 - 16));
		
		add(paddles);
		add(walls);
		add(ball);
	}

	override public function destroy():Void
	{
		super.destroy();
	}

	override public function update():Void
	{
		super.update();

		paddle1.immovable = true;
		paddle2.immovable = true;

		FlxG.collide(ball, paddles);
		FlxG.collide(ball, walls);
		
		paddle1.immovable = false;
		paddle2.immovable = false;

		FlxG.collide(paddles, walls);

		
	}
}