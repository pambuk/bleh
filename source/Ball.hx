package ;

import org.flixel.FlxSprite;
import org.flixel.FlxObject;

class Ball extends FlxSprite
{
	public function new(x:Int, y:Int) {
		super(x, y);

		loadGraphic("assets/sphere.png", false, false, 32, 32);
		velocity.x = 300;
		velocity.y = 200;
		elasticity = 1;
	}

	public function collision(ball:FlxObject, paddle:FlxObject):Void {
		trace("We have a collision");
	}

	// override public function update():Void {

	// }
}