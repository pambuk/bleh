package;

import org.flixel.FlxSprite;
import org.flixel.FlxG;
import org.flixel.FlxObject;

class Paddle extends FlxSprite
{
	private var playerNumber:Int;
	private var speed:Float = 400;

	public function new(x:Int, y:Int, player:Int) {
		super(x, y);
		makeGraphic(15, 80, 0xffffffff);
		playerNumber = player;
		immovable = true;
	}

	override public function update():Void {
		super.update();

		switch (playerNumber) {
			case 1:
				if (FlxG.keys.UP) {
					y -= speed * FlxG.elapsed;
					if (y < 0) {
						y = 0;
					}
				}

				if (FlxG.keys.DOWN) {
					y += speed * FlxG.elapsed;
					if (y > FlxG.height - 80) {
						y = FlxG.height - 80;
					}
				}
			case 2:
				if (FlxG.keys.Q) {
					y -= speed * FlxG.elapsed;
					if (y < 0) {
						y = 0;
					}
				}

				if (FlxG.keys.A) {
					y += speed * FlxG.elapsed;
					if (y > FlxG.height - 80) {
						y = FlxG.height - 80;
					}
				}
		}
	}
}