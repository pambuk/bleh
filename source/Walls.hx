package;

import org.flixel.FlxSprite;

class Walls extends FlxSprite
{
	public function new(x:Int, y:Int, _type:String) {
		super(x, y);

		immovable = true;
		
		switch (_type) {
			case "side":
				makeGraphic(20, 130, 0xff666666);
			case "top-bottom":
				makeGraphic(640, 20, 0xff666666);
		}
	}
}